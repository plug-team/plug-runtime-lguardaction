package plug.languages.guardaction;

import announce4j.Announcer;
import javax.xml.ws.Holder;
import org.junit.Test;
import plug.composite.CompositeRuntime;
import plug.core.IFiredTransition;
import plug.core.ILanguagePlugin;
import plug.core.IRuntimeView;
import plug.core.execution.IExecutionController;
import plug.events.ExecutionEndedEvent;
import plug.explorer.BFSExplorer;
import plug.explorer.ConcurrentBFSExplorer;
import plug.language.guardaction.GuardActionPlugin;
import plug.language.guardaction.examples.GAExamples;
import plug.language.guardaction.runtime.GAConfiguration;
import plug.language.guardaction.runtime.GATransitionRelation;
import plug.statespace.IGraphAccess;
import plug.statespace.SimpleStateSpaceManager;
import plug.statespace.transitions.FiredTransition;
import plug.utils.Pair;
import plug.verifiers.deadlock.DeadlockVerifier;
import plug.verifiers.deadlock.FinalStateDetected;
import plug.verifiers.predicates.PredicateVerifier;
import plug.verifiers.predicates.PredicateViolationEvent;
import plug.verifiers.predicates.PredicatesVerifiedEvent;
import plug.verifiers.predicates.PredicatesViolatedEvent;
import plug.visualisation.StateSpace2TGF;


import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class GuardActionTest {
	ILanguagePlugin module = new GuardActionPlugin();
	@Test
	public void testDifferentRuntimeViews() {
		IRuntimeView view1 = module.getRuntimeView(new GATransitionRelation());
		IRuntimeView view2 = module.getRuntimeView(new GATransitionRelation());
		assertNotSame(view1, view2);
	}

	@Test
	public void testPermutationConcurrent() {
		GATransitionRelation rt = new GATransitionRelation();
		int width = 2;
		int height = 2;
		rt.program = GAExamples.permutations(width, height);

		ConcurrentBFSExplorer controller = new ConcurrentBFSExplorer(rt, new SimpleStateSpaceManager(true), 8);
		controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
			System.out.println("Final size: " + o.getSource().getStateSpaceManager().size() + "\n");
			assertEquals( BigInteger.valueOf(2).pow(width*height).intValue(), o.getSource().getStateSpaceManager().size());
		});

		controller.execute();

		System.out.println("state-space computation done");

		Map<Integer, Integer> histogram = new HashMap<>();

		IGraphAccess graph = controller.getStateSpaceManager().getGraphView();
		Stack<Pair<FiredTransition, Pair<Integer, Iterator<FiredTransition>>>> stack = new Stack<>();
		for (Object o : graph.getInitialVertices()) {
			for (Object e : graph.getOutgoingEdges(o)) {
				stack.push(new Pair<>((FiredTransition) e, new Pair(0, null)));
			}
		}

		BigInteger size = BigInteger.ZERO;
		while (!stack.isEmpty()) {
			Pair<FiredTransition, Pair<Integer, Iterator<FiredTransition>>> node = stack.peek();

			if (node.b.b == null) {
				Collection<FiredTransition> outEdges = graph.getOutgoingEdges(node.a.getTargets().stream().findAny().get());

				if (outEdges.isEmpty()) {
					size = size.add(BigInteger.ONE);
					//System.out.println(size + " " + node.b.a);

					if (size.mod(BigInteger.valueOf(1000000)).equals(BigInteger.ZERO)) {
						System.out.println("Results at: " + size);
						histogram.forEach((k, v) -> System.out.println(k + ";" +v));
					}


					int distance = node.b.a;
					Integer count = histogram.get(distance);
					if (count == null) {
						histogram.put(distance, 1);
					} else {
						histogram.put(distance, ++count);
					}
				}

				node.b.b = outEdges.iterator();
			}

			if (node.b.b.hasNext()) {
				FiredTransition fired = node.b.b.next();
				GAExamples.PermutationBehavior move = (GAExamples.PermutationBehavior)fired.getAction();
				int d = move.manhattanDistanceTo((GAExamples.PermutationBehavior)node.a.getAction());
				int length = node.b.a + d;
				if (length <= ((width * height)-1)*1.5) {
					stack.push(new Pair<>(fired, new Pair<>(length, null)));
				}
			} else {
				stack.pop();
			}
		}

		System.out.println("total;"+ size);
		histogram.forEach((k, v) -> System.out.println(k + ";" +v));

		StateSpace2TGF s2t = new StateSpace2TGF();
		try {
			s2t.toTGF(controller.getStateSpaceManager().getGraphView(), true, true, "result.tgf");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testConcurrentBFS() {
		GATransitionRelation rt = new GATransitionRelation();
		rt.program = GAExamples.parallelCounters(2, 20);

		ConcurrentBFSExplorer controller = new ConcurrentBFSExplorer(rt, new SimpleStateSpaceManager(), 2);
		controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
			System.out.println("Final size: " + o.getSource().getStateSpaceManager().size() + "\n");
			assertEquals(441, o.getSource().getStateSpaceManager().size());
		});

		controller.execute();
	}

	@Test
	public void testOnlyExploration() {
		GATransitionRelation rt = new GATransitionRelation();
		rt.program = GAExamples.parallelCounters(2, 20);

		BFSExplorer controller = new BFSExplorer(rt, new SimpleStateSpaceManager());

		controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
			System.out.println("Final size: " + o.getSource().getStateSpaceManager().size() + "\n");
			assertEquals(441, o.getSource().getStateSpaceManager().size());
		});

		controller.execute();
	}
	

	@Test
	public void testOnlyExplorationTwoRuntimes() {
		GATransitionRelation rt = new GATransitionRelation();
		rt.program = GAExamples.parallelCounters(2, 20);
		CompositeRuntime compositeRuntime = new CompositeRuntime();
		compositeRuntime.addRuntime("ga1", rt);
		compositeRuntime.addRuntime("ga2", rt.createCopy());
		BFSExplorer controller = new BFSExplorer(compositeRuntime, new SimpleStateSpaceManager());

		controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
			System.out.println("Final size: " + o.getSource().getStateSpaceManager().size() + "\n");
			assertEquals(194481, o.getSource().getStateSpaceManager().size());
		});

		controller.execute();
	}

	@Test
	public void testDeadlock() {
		GATransitionRelation runtime = new GATransitionRelation(GAExamples.parallelCounters(2, 10));
		BFSExplorer controller = new BFSExplorer(runtime, new SimpleStateSpaceManager());

		controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
			System.out.println("Final size: " + o.getSource().getStateSpaceManager().size() + "\n");
			assertEquals(121, o.getSource().getStateSpaceManager().size());
		});


		DeadlockVerifier dV = new DeadlockVerifier(controller.getAnnouncer());

		Holder<Boolean> hasDeadlock = new Holder<>(false);
		dV.announcer.when(FinalStateDetected.class, (ann, ev) -> {
			System.out.println("Final state detected: " + ev.getFinalState() );
			hasDeadlock.value = true;
		});

		controller.execute();

		assertEquals(false, hasDeadlock.value);
	}

	@Test
	public void testPredicate1() {
		GATransitionRelation runtime = new GATransitionRelation(GAExamples.parallelCounters(2, 4));

		BFSExplorer controller = new BFSExplorer(runtime, new SimpleStateSpaceManager());
		controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
			System.out.println("Final size: " + o.getSource().getStateSpaceManager().size() + "\n");
			assertEquals(25, o.getSource().getStateSpaceManager().size());
		});

		Holder<Boolean> violated = registerPredicateVerifier(controller);

		controller.execute();

		assertEquals(false, violated.value);
	}

	@Test
	public void testPredicate2() {
		GATransitionRelation runtime = new GATransitionRelation(GAExamples.parallelCounters(2, 5));
		BFSExplorer controller = new BFSExplorer(runtime, new SimpleStateSpaceManager());

		Holder<Boolean> violated = registerPredicateVerifier(controller);
		controller.execute();
		assertEquals(true, violated.value);
	}

	protected Holder<Boolean> registerPredicateVerifier(IExecutionController controller) {
		PredicateVerifier<GAConfiguration> pV = new PredicateVerifier<>(controller.getAnnouncer());

		pV.predicates.add(c ->  !((c.getValues()[0] < 0) || (c.getValues()[0] > 4)));

		Holder<Boolean> violated = new Holder<>(false);

		pV.announcer.when(PredicateViolationEvent.class, (announcer, event) -> {
			System.err.println("predicate violated: " + event.getConfiguration() + " -- " + event.getViolated());
			violated.value = true;
		});
		pV.announcer.when(PredicatesVerifiedEvent.class, (announcer, event) -> {
			System.err.println("predicate verified ! ");
		});
		pV.announcer.when(PredicatesViolatedEvent.class, (announcer, event) -> {
			System.err.println("at least one predicate violated ! ");});
		return violated;
	}
}

