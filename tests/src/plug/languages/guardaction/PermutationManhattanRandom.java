package plug.languages.guardaction;

import plug.language.guardaction.examples.GAExamples;
import sun.util.resources.cldr.zh.CalendarData_zh_Hans_HK;

import java.math.BigInteger;
import java.util.*;

public class PermutationManhattanRandom {

    static int height;
    static int width;


    public int x(int id) {
        return id / height;
    }
    public int y(int id) {
        return id % height;
    }

    public int manhattanDistance(int a, int b) {
        return Math.abs(x(a) - x(b)) + Math.abs(y(a) - y(b));
    }

    public int cost(byte[] permutation) {
        int distance = 0;
        for (int i=0; i<permutation.length-1; i++) {
            byte a = permutation[i];
            byte b = permutation[i+1];
            distance += manhattanDistance(a, b);
        }
        return distance;
    }

    Random random = new Random();
    public byte[] permutation(List<Byte> candidates) {
        byte [] result = new byte[candidates.size()];
        int size = candidates.size();
        for (int i = 0; i<size; i++) {
            int elementID = random.nextInt(candidates.size());
            result[i] = candidates.remove(elementID);
        }
        return result;
    }

    void generate(BigInteger population) {
        int n = width * height;
        List<Byte> base = new ArrayList<>(n);
        for (byte i =0 ; i<n; i++) {
            base.add(i);
        }

        Map<Integer, Integer> histogram = new HashMap<>();
        Set<byte[]> solutions = new HashSet<>();

        for (BigInteger i = BigInteger.ZERO; i.compareTo(population) < 0; i = i.add(BigInteger.ONE)) {
            byte[] permutation;
            do {
                List<Byte> candidate = (List<Byte>) ((ArrayList<Byte>) base).clone();
                permutation = permutation(candidate);
            } while (solutions.contains(permutation));
            solutions.add(permutation);

            int length = cost(permutation);

            Integer count = histogram.get(length);
            if (count == null) {
                histogram.put(length, 1);
            } else {
                histogram.put(length, ++count);
            }

            if (i.mod(BigInteger.valueOf(10000000)).equals(BigInteger.ZERO)) {
                System.out.println("Results at: " + i);
                histogram.forEach((k, v) -> System.out.println(k + ";" +v));
            }
        }

        histogram.forEach((k, v) -> System.out.println(k + ";" +v));

    }

    public static void main(String args[]) {
        height = 5;
        width = 5;

       new PermutationManhattanRandom().generate(BigInteger.valueOf(1000000000));
    }
}
