package plug.language.guardaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import plug.core.IRuntimeView;
import plug.core.view.ConfigurationItem;
import plug.language.guardaction.model.GABehavior;
import plug.language.guardaction.runtime.GAConfiguration;
import plug.language.guardaction.runtime.GATransitionRelation;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class GuardActionRuntimeView implements IRuntimeView<GAConfiguration, GABehavior> {
    GATransitionRelation runtime;

    public GuardActionRuntimeView(GATransitionRelation runtime) {
        this.runtime = runtime;
    }

    @Override
    public GATransitionRelation getRuntime() {
        return runtime;
    }

	@Override
	public List<ConfigurationItem> getConfigurationItems(GAConfiguration value) {
		List<ConfigurationItem> variableList = new ArrayList<>();
		int idx = 0;
        for (Map.Entry<String, Integer> entry : runtime.program.getVariables().entrySet()) {
        		variableList.add(new ConfigurationItem("variable", entry.getKey() + " = " + value.getValues()[idx], null, null));
            idx++;
        }
		
		return variableList;
	}

	@Override
	public String getFireableTransitionDescription(GABehavior transition) {
		return transition.toString();
	}
}
