package plug.language.guardaction;

import plug.core.ILanguageLoader;
import plug.core.ILanguagePlugin;
import plug.core.IRuntimeView;
import plug.language.guardaction.runtime.GATransitionRelation;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class GuardActionPlugin implements ILanguagePlugin<GATransitionRelation> {
    ILanguageLoader loader = new GuardActionLoader();

    @Override
    public String[] getExtensions() {
        return new String[0];
    }

    @Override
    public String getName() {
        return "GuardAction";
    }

    @Override
    public ILanguageLoader getLoader() {
        return loader;
    }

    @Override
    public IRuntimeView getRuntimeView(GATransitionRelation runtime) {
        return new GuardActionRuntimeView(runtime);
    }
}
