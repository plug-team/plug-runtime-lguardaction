package plug.language.guardaction.runtime;

import plug.core.IConcurrentTransitionRelation;
import plug.core.IFiredTransition;
import plug.language.guardaction.model.GABehavior;
import plug.language.guardaction.model.GAProgram;
import plug.statespace.transitions.FiredTransition;

import java.util.*;

public class GATransitionRelation implements IConcurrentTransitionRelation<GATransitionRelation, GAConfiguration, GABehavior> {
	public GAProgram program;

	public GATransitionRelation(){};
	public GATransitionRelation(GAProgram program) {
		this.program = program;
	}

	@Override
	public GATransitionRelation createCopy() {
		GATransitionRelation runtime = new GATransitionRelation();
		runtime.program = program.copy();
		return runtime;
	}
	
	@Override
	public Set<GAConfiguration> initialConfigurations() {
		Map<String, Integer> variables = program.getVariables();
		int values[] = new int[variables.size()];
		
		String keys[] = variables.keySet().toArray(new String[values.length]);
		for (int idx = 0; idx < values.length; idx++) {
			values[idx] = variables.get(keys[idx]);
			variables.put(keys[idx], idx);
		}
		
		GAConfiguration config = new GAConfiguration();
		config.values = values;
		return new HashSet<GAConfiguration>() {{ add(config); }};
	}

	@Override
	public Collection<GABehavior> fireableTransitionsFrom(GAConfiguration configuration) {
		List<GABehavior> fireables = new ArrayList<>();
		for (GABehavior behavior : program.getBehaviors()) {
			if (behavior.test(configuration.values)) {
				fireables.add(behavior);
			}
		}
		return fireables;
	}

	@Override
	public IFiredTransition<GAConfiguration, ?> fireOneTransition(GAConfiguration source, GABehavior transition) {
		GAConfiguration myNewState = source.createCopy();
		transition.action(myNewState.values);
		
		return new FiredTransition<>(source, myNewState, transition);
	}
}
