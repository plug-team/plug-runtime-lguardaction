package plug.language.guardaction.runtime;

import java.util.Arrays;
import plug.core.defaults.DefaultConfiguration;

public class GAConfiguration extends DefaultConfiguration<GAConfiguration> {
	int[] values;

	public int[] getValues() {
		return values;
	}

	@Override
	public GAConfiguration createCopy() {
		GAConfiguration newC = new GAConfiguration();
		newC.values = Arrays.copyOf(values, values.length);
		return newC;
	}
	
	@Override
	public int hashCode() {
		int code = 17;
		for ( int i=0; i<values.length; i++ ) {
			code = code * 37 + (i+1)*values[i];
		}
		
		return code;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj instanceof GAConfiguration) {
			GAConfiguration other = (GAConfiguration) obj;
			if (Arrays.equals(values, other.values)) return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return Arrays.toString(values);
	}
}
