package plug.language.guardaction;

import java.net.URI;
import java.util.Map;
import plug.core.ILanguageLoader;
import plug.language.guardaction.runtime.GATransitionRelation;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class GuardActionLoader implements ILanguageLoader<GATransitionRelation> {

    @Override
    public GATransitionRelation getRuntime(URI modelURI, Map<String, Object> options) {
        return null;
    }
}
