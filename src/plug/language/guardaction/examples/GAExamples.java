package plug.language.guardaction.examples;

import plug.language.guardaction.model.GABehavior;
import plug.language.guardaction.model.GAProgram;

public class GAExamples {

	public static class PermutationBehavior extends GABehavior {
		int id;
		int width;
		int height;

		public PermutationBehavior(String name, int id, int width, int height) {
			super(name);
			this.id = id;
			this.width = width;
			this.height = height;
		}

		@Override
		public boolean test(int env[]) {
			return resolve("v"+id, env) != 1;
		}

		@Override
		public void action(int env[]) {
			update("v"+id, 1, env);

		}

		public int x() {
			return id / height;
		}
		public int y() {
			return id % height;
		}

		public int manhattanDistanceTo(PermutationBehavior other) {
			return Math.abs(this.x() - other.x()) + Math.abs(this.y() - other.y());
		}

		@Override
		public String toString() {
			return name + "["+ x() + "@" + y() +"]";
		}
	}

	public static GAProgram permutations(final int width, final int height) {
		final GAProgram program = new GAProgram();
		int size = width * height;
		for (int i = 0; i<size; i++) {
			program.addVariable("v" + i, 0);

			PermutationBehavior move = new PermutationBehavior("move", i, width, height);
			program.addBehavior(move);
		}
		return program;
	}

	public static GAProgram counterReset(final int max) {
		final GAProgram program = new GAProgram();
		program.addVariable("counter", 0);
		
		GABehavior inc = new GABehavior("inc") {
			
			@Override
			public boolean test(int env[]) {
				return resolve("counter", env) < max;
			}
			
			@Override
			public void action(int env[]) {
				update("counter", resolve("counter", env) + 1, env);
				
			}
		};
		
		GABehavior reset = new GABehavior("reset") {
			
			@Override
			public boolean test(int env[]) {
				return resolve("counter", env) >= max;
			}
			
			@Override
			public void action(int env[]) {
				update("counter", 0, env);
				
			}
		};
		
		program.addBehavior(inc);
		program.addBehavior(reset);
		
		return program;
	}
	
	public static GAProgram parallelCounters(int nbCounters, final int max) {
		final GAProgram program = new GAProgram();
		
		for (int i = 0; i<nbCounters; i++) {
			final int idx = i;
		
			program.addVariable("counter"+idx, 0);
			
			GABehavior inc = new GABehavior("inc"+idx) {
				
				@Override
				public boolean test(int env[]) {
					return resolve("counter"+idx, env) < max;
				}

				@Override
				public void action(int env[]) {
					update("counter"+idx, resolve("counter"+idx, env) + 1, env);

				}
			};
			
			GABehavior reset = new GABehavior("reset" + idx) {
				
				@Override
				public boolean test(int env[]) {
					return resolve("counter"+idx, env) >= max;
				}
				
				@Override
				public void action(int env[]) {
					update("counter"+idx, 0, env);
					
				}
			};
			
			program.addBehavior(inc);
			program.addBehavior(reset);
		}
		return program;
	}
}
