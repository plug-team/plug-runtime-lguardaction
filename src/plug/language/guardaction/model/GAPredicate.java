package plug.language.guardaction.model;

import plug.language.guardaction.model.GAProgram;

import java.util.function.Predicate;

public abstract class GAPredicate implements Predicate<int[]> {
	GAProgram parent;
	protected int resolve(String name, int values[]) {
		return parent.resolve(name, values);
	}
}
