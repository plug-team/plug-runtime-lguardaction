package plug.language.guardaction.model;


public abstract class GABehavior extends GAPredicate {
	public String name;

	public GABehavior(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	protected void update(String string, int i, int values[]) {
		parent.update(string, i, values);
	}
	
	public abstract void action(int env[]);

	@Override
	public boolean test(int[] configuration) {
		return false;
	}
}
