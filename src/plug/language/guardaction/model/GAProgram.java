package plug.language.guardaction.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GAProgram {
	Map<String, Integer> variables = new LinkedHashMap<>();
	List<GABehavior> behaviors = new ArrayList<>();

	public Map<String, Integer> getVariables() {
		return variables;
	}

	public List<GABehavior> getBehaviors() {
		return behaviors;
	}

	public void addVariable(String name, int initialValue) {
		variables.put(name, initialValue);
	}
	
	public void addBehavior(GABehavior beh) {
		beh.parent = this;
		behaviors.add(beh);
	}
	
	int resolve(String name, int values[]) {
		return values[variables.get(name)];
	}

	public void update(String string, int i, int values[]) {
		values[variables.get(string)] = i;
	}
	
	public GAProgram copy() {
		GAProgram p = new GAProgram();
		p.behaviors = new ArrayList<>(behaviors);

		for (GABehavior b : p.behaviors) {
			b.parent = p;
		}
		p.variables = variables;
		return p;
	}
}
